#include <iostream>
using namespace std;

// Program Perkalian Segitiga
//// Membuat Hasil Perkalian Segitiga 2 Dimensi

void perkalianSegitiga(int n) {
    /*
        Prosedur (void) menampilkan perkalian segitiga

        input: n:int
    */

    // input baris
    for (int i = 1; i <= n; i++) {
        // init kolom
        string kolom = "";

        for (int j = 1; j <= n; j++) {
            int hasil_perkalian;

            // Untuk perkalian di bawah seratus
            //// akan ditambah gap 2 spasi untuk hasil perkalian di bawah 100
            //// karena terdapat dua angka yang menghasilkan angka ratusan
            //// atau mungkin puluhan untuk perkalian di bawah sepuluh
            if (n < 100) {
                if (i >= j) {
                    hasil_perkalian = i * j;
                    kolom += to_string(hasil_perkalian);

                    if (hasil_perkalian < 10) {
                        kolom += "   ";
                    } else if (hasil_perkalian < 100) {
                        kolom += "  ";
                    } else {
                        kolom += " ";
                    }
                }

            }
        }

        cout << kolom << endl;
    }
}

int main() {
    // input angka
    int N;
    cout << "Masukkan angka: ";
    cin >> N;

    // validasi untuk angka minimal 100
    while (N >= 100) {
        cout << "Angka setidaknya harus di bawah 100" << endl;
        cout << "Masukkan angka: ";
        cin >> N;
    }

    perkalianSegitiga(N);

    return 0;
}