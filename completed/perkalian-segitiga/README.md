# Perkalian Segitiga

## Sumber Soal Tiktok

https://www.tiktok.com/@wahyuptama/video/7173136558587972890

## Pengantar

Soal atau tantangan ini diambil dari akun TikTok [@wahyuptama](https://www.tiktok.com/@wahyuptama)  dan terlampir dari gambar pada `soal.jpg`. Berdasarkan soal tersebut, kita diminta untuk membuat output dari perkalian segitiga hingga angka 8.

Pada kali ini, kita akan memodifikasi soal tersebut dengan **membuat hasil perkalian segitiga berdasarkan input dari user**. Adapun ini sudah ada pada postingan TikTok beliau, namun kita akan sedikit merapikan hasil outputnya serta memberi **batasan input hingga bilangan 100**

## Bahasa yang Digunakan

- [Python](https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/perkalian-segitiga/perkalian-segitiga.py)

- [C++](https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/perkalian-segitiga/perkalian-segitiga.cpp)

## Input

```
Masukkan angka: 11
```

## Output

```
1   
2   4   
3   6   9   
4   8   12  16  
5   10  15  20  25  
6   12  18  24  30  36  
7   14  21  28  35  42  49  
8   16  24  32  40  48  56  64  
9   18  27  36  45  54  63  72  81  
10  20  30  40  50  60  70  80  90  100 
11  22  33  44  55  66  77  88  99  110 121
```






