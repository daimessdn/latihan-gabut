# Program Perkalian Segitiga
## Membuat Hasil Perkalian Segitiga 2 Dimensi

def perkalian_segitiga(n):
    """
    Prosedur (void) menampilkan perkalian segitiga

    input: n:int
    """

    # input baris
    for i in range(1, n + 1):
        kolom = ""

        # input kolom
        for j in range(1, n + 1):
            # Untuk perkalian di bawah seratus
            ## akan ditambah gap 2 spasi untuk hasil perkalian di bawah 100
            ## karena terdapat dua angka yang menghasilkan angka ratusan
            ## atau mungkin puluhan untuk perkalian di bawah sepuluh
            if (N < 100):
                if (i >= j):
                    hasil_perkalian = i * j
                    kolom += str(hasil_perkalian)

                    if (hasil_perkalian < 10):
                        kolom += "   "
                    elif (hasil_perkalian < 100):
                        kolom += "  "
                    else:
                        kolom += " "

        print(kolom)

    return

def perkalian_segitiga_2(n):
    """
    Cara lain dari fungsi sebelumnya
    """

    # input baris
    for i in range(1, n + 1):
        kolom = ""

        for j in range(1, n + 1):
            if (N < 100):
                if (i >= j):
                    hasil_perkalian = i * j
                    kolom = kolom + str(hasil_perkalian) + (
                        "   " if hasil_perkalian < 10 else (
                            "  " if hasil_perkalian < 100 else " "
                        )
                    )

        print(kolom)

    return


# input angka
N = int(input("Masukkan angka: "))

# validasi untuk angka minimal 100
while (N >= 100):
    print("Angka setidaknya harus di bawah 100")
    N = int(input("Masukkan angka: "))

# perkalian_segitiga(N)
perkalian_segitiga_2(N)