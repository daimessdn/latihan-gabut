# Program Kalkulator CMD
## Membuat Kalkulator berdasarkan command prompt

"""
DOKUMENTASI

add      : penjumlahan
subtract : pengurangan
multiply : perkalian
division : pembagian
modulo   : melihat sisa pembagian
help     : bantuan
exit     : keluar
"""

def add(args):
    """
    Fungsi penjumlahan
    """

    result = 0

    for i in args:
        result += i

    return result

def subtract(args):
    """
    Fungsi pengurangan
    """

    result = 0
    
    for i in args:
        result -= i

    return result

def multiply(args):
    """
    Fungsi perkalian
    """

    result = 0
    
    for i in args:
        result *= i

    return result


def division(args):
    """
    Fungsi pembagian
    """

    try:
        result = 0
        
        for i in args:
            result /= i

        return result

    except ZeroDivisionError:
        return "Error: tidak bisa dibagi dengan 0"

def modulo(args):
    """
    Fungsi modulo
    """

    try:
        return args[0] % args[1]

    except:
        return "Error: harus ada 2 argumen."

def help():
    print("Berikut command yang bisa digunakan:\n")
    print("add      : menambah bilangan.")
    print("subtract : mengurangi bilangan.")
    print("multiply : mengalikan bilangan.")
    print("divide   : membagi bilangan.")
    print("modulo   : melihat sisa pembagian bilangan.")
    print("exit     : keluar dari kalkulator command prompt.\n")

    print("Kamu bisa cek masing-masing cara pakai command dengan \"help nama_command\"\n")

def read_command(command):
    global cmd_start

    command = command.split(" ")
    base_command = command[0]
    args = command[1:]

    # command buat keluar dari program
    if (base_command == "exit"):
        print("< Dadah :)")
        cmd_start = False

    # bantuan dan bantuan masing-masing command
    elif (base_command == "help"):
        if (len(args) > 1):
            print("< Error: terlalu banyak argumen.")
            
        elif (len(args) == 1):
            if (args[0] == "add"):
                print("\nadd: menambah bilangan sebanyak N argumen")
                print("\n  > add [...N]")
                print("\nMisalnya:")
                print("  > add 1 2 3")
                print("  < 6\n")

            elif (args[0] == "subtract"):
                print("\nsubtract: mengurangi bilangan sebanyak N argumen")
                print("\n  > subtract [...N]")
                print("\nMisalnya:")
                print("  > subtract 10 3 1 5")
                print("  < 1\n")

            elif (args[0] == "multiply"):
                print("\nmultiply: mengalikan bilangan sebanyak N argumen")
                print("\n  > multiply [...N]")
                print("\nMisalnya:")
                print("  > multiply 10 3 1 5")
                print("  < 150\n")

            elif (args[0] == "divide"):
                print("\ndivide: membagi bilangan sebanyak N argumen")
                print("\n  > divide [...N]")
                print("\nMisalnya:")
                print("  > divide 10 5")
                print("  < 2\n")

            elif (args[0] == "modulo"):
                print("\nmodulo: melihat sisa pembagian 2 bilangan (hanya bisa 2 argumen)")
                print("\n  > modulo n1 n2")
                print("\nMisalnya:")
                print(" > modulo 10 4")
                print(" < 2\n")

            elif (args[0] == "exit"):
                print("\exit: keluar dari program\n")

            else:
                print("< help: tidak ada bantuan untuk command yang dimaksud.")

        else:
            help()

    # command penjumlahan
    elif (base_command == "add"):
        try:
            if (len(args) >= 2):
                print("< " + str(add([int(x) for x in args])))

            else:
                print("< Error: setidaknya harus ada 2 arguman")

        except ValueError:
            print("< Error: argumen tidak valid, harus berupa angka.")

    # command pengurangan
    elif (base_command == "subtract"):
        try:
            if (len(args) >= 2):
                print("< " + str(subtract([int(x) for x in args])))

            else:
                print("< Error: setidaknya harus ada 2 arguman")

        except ValueError:
            print("< Error: argumen tidak valid, harus berupa angka.")

    # command perkalian
    elif (base_command == "multiply"):
        try:
            if (len(args) >= 2):
                print("< " + str(multiply([int(x) for x in args])))

            else:
                print("< Error: setidaknya harus ada 2 arguman")

        except ValueError:
            print("< Error: argumen tidak valid, harus berupa angka.")

    # command pembagian
    elif (base_command == "division"):
        try:
            if (len(args) >= 2):
                print("< " + str(division([int(x) for x in args])))

            else:
                print("< Error: setidaknya harus ada 2 arguman")

        except ValueError:
            print("< Error: argumen tidak valid, harus berupa angka.")

    # command sisa pembagian
    elif (base_command == "modulo"):
        try:
            if (len(args) > 2):
                print("< Error: terlalu banyak argumen")
            
            else:
                print("< " + str(modulo([int(x) for x in args])))

        except ValueError:
            print("< Error: argumen tidak valid, harus berupa angka.")
    
    else:
        print("< " + base_command + ": command not found.")

    return

# init mulai terminal
cmd_start = True

print("Selamat datang di kalkulator command prompt.\n\
Ketik \"help\" untuk bantuan.\n\
Kamu bisa keluar dengan command \"exit\" atau Ctrl + C\n")

# sesi termial dimulai
while (cmd_start == True):
    try:
        command = str(input("> "))
        read_command(command)

    except KeyboardInterrupt:
        print("")
        read_command("exit")