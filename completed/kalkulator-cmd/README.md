# Kalkulator Command Prompt

## Gambaran Umum

Kalkulator mempermudah kita dalam melakukan suatu perhitungan. Pada persoalan kali ini, kita akan membuat kalkulator dengan menggunakan input menyerupai *command prompt* dengan fitur *command* sebagai berikut.

1. `help`: Melihat bantuan

2. `add n1 n2 ... N`: Penjumlahan bilangan sebanyan N argumen

3. `subtract n1 n2 ... N`: Penguarangan bilangan sebanyan N argumen

4. `multiply n1 n2 ... N`: Perkalian bilangan sebanyan N argumen

5. `divide n1 n2 ... N`: Pembagian bilangan sebanyan N argumen

6. `modulo n1 n2`: Melihat sisa pembagian dengan bilangan

7. `exit`: Keluar dari program

## Output

Terlampir pada video TikTok oleh [@d.ganteng21](https://www.tiktok.com/@d.ganteng21/video/7185315027891539226)

## Bahasa yang Digunakan

Python
