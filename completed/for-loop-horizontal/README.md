# For-Loop Horizontal

Pada persoalan ini, kita akan membuat array number untuk bisa diprint secara horizontal.
Pada referensi (soal) sebelumnya yang saya temukan melalui grup komunitas (hanya sebagai contoh), perintah for Loop
dicetak secara vertikal dengan cara berikut (ditulis dengan Python).

```python
for i in range(1, 11):
    print(i)
```

Sehingga diperoleh output sebagai berikut.

```
1
2
3
4
5
6
7
8
9
10
```

Pada kasus ini, kita akan membuat array yang bisa diprint secara horizontal menggunakan dua cara:

1. **Cara manual**: dengan menuliskan algoritma dengan fungsi dasar, ditulis sebagai fungsi `for_loop_horizontal`.
2. **Cara korespondensi**: cara cepat, dengan menggunakan fungsi `join()` dan korespondensi array pada angka, ditulis sebagai fungsi `for_loop_horizontal_2` (khusus Python).

### Input (Python)

```
Masukkan bilangan bulat positif: 10
```

### Output (Python)

```
Cara manual: 1 2 3 4 5 6 7 8 9 10 
Cara cepet : 1 2 3 4 5 6 7 8 9 10
```

### Source code

#### Python

[https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/for-loop-horizontal/for-loop-horizontal.py](https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/for-loop-horizontal/for-loop-horizontal.py)


#### C++

[https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/for-loop-horizontal/for-loop-horizontal.cpp](https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/for-loop-horizontal/for-loop-horizontal.cpp)
