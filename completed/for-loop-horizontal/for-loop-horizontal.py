def for_loop_horizontal(n):
    """
    Fungsi looping angka secara horizontal
    Menggunakan algoritma manual
    Dimulai dari angka 1 hingga angka n

    input: n:int
    result: horizontal_result:str
    """

    # init string
    horizontal_result = ""

    # mengisi angka dengan
    ## variabel string horizontal 
    for i in range(1, n + 1):
        horizontal_result += str(i)

        # penambahan spasi
        if (i < n + 1):
            horizontal_result += " "

    return horizontal_result

def for_loop_horizontal_2(n):
    """
    Fungsi looping angka secara horizontal
    Menggunakan korespondensi array (cara cepat)
    Dimulai dari angka 1 hingga angka n

    input: n:int
    result: horizontal_result:str
    """

    return " ".join([str(x) for x in range(1, n + 1)])

N = int(input("Masukkan bilangan bulat positif: "))

while (N <= 0):
    print("Bukan bilangan bulat positif.")
    N = int(input("Masukkan bilangan bulat positif: "))

print("Cara manual:", for_loop_horizontal(N))
print("Cara cepet :", for_loop_horizontal_2(N))