#include <iostream>
using namespace std;

/*
    Fungsi For Loop Horizontal
    Fungsi untuk mencetak array number secara horizontal
    input: n:integer
    output: result: string
*/
string forLoopHorizontal(int n) {
    // init string horizontal
    string result = "";

    // mulai mengisi array horizontal
    for (int i = 1; i < n + 1; i++) {
        result = result + to_string(i);
        /*
            Sebagai catatan, fungsi to_string merupakan
            fungsi bawaan C++ untuk mengubah int menjadi string
        */

        // penambahan spasi
        if (i < (n + 1)) {
            result = result + " ";
        }
    }

    return result;
}

/*
    Fungsi For Loop Horizontal
    Fungsi untuk mencetak array number secara horizontal
    input: n:integer
    output: karena merupakan fungsi void, maka tidak mereturn apapun
*/
void forLoopHorizontal2(int n) {
    // langsung mencetak array horizontal
    cout << "Hasil dengan fungsi void: ";

    for (int i = 1; i < n + 1; i++) {
        cout << i;

        // penambahan spasi
        if (i < (n + 1)) {
            cout << " ";
        }
    }

    cout << endl;
}

int main() {
    // input angka
    int N;

    cout << "Masukkan bilangan bulat positif: ";
    cin >> N;

    while (N <= 0) {
        cout << "Bukan termasuk bilangan bulat positif." << endl << endl;

        cout << "Masukkan bilangan bulat positif: ";
        cin >> N;
    }

    // Hasil dengan fungsi return
    cout << "Hasil dengan return: " << forLoopHorizontal(N) << endl;

    // Hasil dengan fungsi void (prosedur)
    forLoopHorizontal2(N);

    return 0;
}
    