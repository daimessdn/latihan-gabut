      program FORH

c     program mencetak print loop horizontal

      integer i, j
      integer a(5, 10)

c     output = ''

c     do i = 1, 9
c           output = output // char(48 + i) // ' '
c           write(*, *) trim(output)
c     end do

c     write(*, *) trim(output)

      do 30, i = 1,5
            do 40, j = 1,10
                  a(i, j) = ((i-1) * 10) + j
  40        continue
  30  continue

      do 10 i = 1, 5
            write(*, 1000) (a(i,j), j=1,10)
  10  continue
1000  format (I2)

      stop
      end