class Buku:
    def __init__(self, judul, pengarang, penerbit, tahun_terbit):
        self.judul = judul
        self.pengarang = pengarang
        self.penerbit = penerbit
        self.tahun_terbit = tahun_terbit
        self.status_pinjam = False

    def tampilkan_info(self):
        print("Judul:", self.judul)
        print("Pengarang:", self.pengarang)
        print("Penerbit:", self.penerbit)
        print("Tahun Terbit:", self.tahun_terbit)
        print("Status Pinjam:", "Dipinjam" if self.status_pinjam else "Tersedia")

    def pinjam(self):
        self.status_pinjam = True

    def kembalikan(self):
        self.status_pinjam = False

class Perpustakaan:
    def __init__(self, daftar_buku = []):
        self.daftar_buku = daftar_buku

    def tambah_buku(self, buku_baru):
        self.daftar_buku.append(buku_baru)

    def cari_buku(self, judul):
        for buku in self.daftar_buku:
            if buku.judul.lower() == judul.lower():
                return buku

            return None
        
    def tampilkan_daftar_buku(self):
        for buku in self.daftar_buku:
            buku.tampilkan_info()
            print()

buku1 = Buku("Pemrograman Python", "John Doe", "Gramedia", 2022)
buku2 = Buku("Kisah Pahlawan Kemerdekaan", "Jane Doe", "Erlangga", 2021)

daftar_buku = [buku1, buku2]
perpustakaan = Perpustakaan(daftar_buku)

buku3 = Buku("Statistika Dasar", "Adam Smith", "Andi Offset", 2022)
perpustakaan.tambah_buku(buku3)

perpustakaan.tampilkan_daftar_buku()


judul_buku = "pemrograman python"
buku_dicari = perpustakaan.cari_buku(judul_buku)

if (buku_dicari):
    print("Buku ditemukan:")
    buku_dicari.tampilkan_info()
else:
    print("Buku tidak ditemukan.")

buku_dicari.pinjam()
print("Buku", buku_dicari.judul, "dipinjam.")
buku_dicari.tampilkan_info()

buku_dicari.kembalikan()
print("Buku", buku_dicari.judul, "dikembalikan.")
buku_dicari.tampilkan_info()
