"""
Collatz Conjecture

Merupakan salah satu permasalahan matematika yang sulit dipecahkan.
Permasalahan ini didefinisikan dengan fungsi
f(x) = 3 * x + 1 untuk x mod (2) != 0
f(x) = x / 2     untuk x mod (2) = 0

Fungsi ini akan berjalan secara terus-menerus hingga x mencapai 1.

Referensi: https://en.wikipedia.org/wiki/Collatz_conjecture
"""

def collatz_conjecture(x):
    """
    Fungsi Collatz
    """

    if (x % 2 == 0):
        return x // 2
    else:
        return 3 * x + 1
    
def main():
    # inisiasi sekuens bilangan
    sequence = []
    N = int(input("Masukkan bilangan N: "))

    while (N != 1):
        sequence.append(N)
        N = collatz_conjecture(N)

    sequence.append(N)
    print(sequence)

if (__name__ == "__main__"):
    main()