/*
Collatz Conjecture

Merupakan salah satu permasalahan matematika yang sulit dipecahkan.
Permasalahan ini didefinisikan dengan fungsi
f(x) = 3 * x + 1 untuk x mod (2) != 0
f(x) = x / 2     untuk x mod (2) = 0

Fungsi ini akan berjalan secara terus-menerus hingga x mencapai 1.

Referensi: https://en.wikipedia.org/wiki/Collatz_conjecture
*/

const readline = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout
});

const collatz_conjecture = (x) => {
    // Fungsi Collatz
    
    if (x % 2 == 0) { return x / 2; }
    else { return 3 * x + 1; };
}
    
// inisiasi sekuens bilangan
const sequence = []

readline.question(
    "Masukan bilangan n: ", (n) => {
        let num = parseInt(n);

        while (num != 1) {
            sequence.push((num));
            num = collatz_conjecture(num);
        }
        
        sequence.push(num);
        console.log(sequence);

        readline.close();
    }
);
