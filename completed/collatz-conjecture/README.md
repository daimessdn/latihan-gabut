# Collatz Conjecture

Merupakan salah satu permasalahan matematika yang sulit dipecahkan. Permasalahan ini didefinisikan dengan fungsi

$`f(x) = \begin{cases} 3x + 1  &\text{for } x \ mod(2)\neq 0 \\ \frac{n}{2} &\text{for } x \ mod(2) = 0 \end{cases}`$

Fungsi ini akan berjalan secara terus-menerus hingga x mencapai 1.

Referensi: https://en.wikipedia.org/wiki/Collatz_conjecture