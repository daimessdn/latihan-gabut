def jumlah_kuadrat_pertama(N):
    """
    Menjumlahkan N kuadrat pertama
    Input: N:int
    Output: total:int
    """

    # init total
    total = 0

    # perulangan dengan for loop
    for i in range(1, N+1):
        # penjumlahan dengan kuadrat bilangan i
        ## dengan i^2 = i * i
        total += i * i

    return total

if (__name__ == "__main__"):
    N = int(input("Masukkan bilangan bulat positif: "))

    # validasi input (jika N bilangan bula positif)
    if (N >= 1):
        print("Hasilnya:", jumlah_kuadrat_pertama(N));
    else:
        print("Input tidak valid")