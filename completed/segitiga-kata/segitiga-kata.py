# Program Segitiga Kata
## Membuat kata menjadi pola segitiga yang menarik

def segitiga_sumber(kata):
    """
        Prosedur (void) segitiga kata berdasarkan sumber soal (TikTok)
        
        input: kata:str
        output: None (void)
    """

    print("\nBerdasarkan kodingan dari sumber:\n")

    x = 0

    for i in kata:
        x += 1
        print(kata[0:x])

    for i in kata:
        x -= 1
        print(kata[0:x])

    return

def segitiga_terbalik(kata):
    """
        Sekarang mari kita buat segitiga python secara terbalik
        dengan urutan kata yang sama dan input yang sama
    """

    print("\nHasil segitiga terbalik:\n")

    x = 0
    panjang_kata = len(kata)

    for i in kata:
        x += 1
        print((" " * (panjang_kata - x)) + kata[0:x])

    for i in kata:
        x -= 1
        print((" " * (panjang_kata - x)) + kata[0:x])

    return

def segitiga_spasi_rata_tengah(kata):
    """
        Sekarang mari kita buat segitiga secara vertikal
        dengan spasi dan rata tengah
        dengan urutan kata yang sama dan input yang sama
    """

    print("\nHasil segitiga rata tengah:\n")

    x = 0
    panjang_kata = len(kata)

    for i in kata:
        x += 1
        print((" " * (panjang_kata - x)) + (" ".join(kata[0:x])))

    return

def segitiga_spasi_rata_tengah_terbalik(kata):
    """
        Sekarang mari kita terapkan fungsi
        segitiga_spasi_rata_tengah secara terbalik
        dengan urutan kata yang sama dan input yang sama
    """

    print("\nHasil segitiga rata tengah terbalik:\n")

    x = 0
    panjang_kata = len(kata)

    for i in kata:
        x += 1
        print((" " * (x)) + (" ".join(kata[0:(panjang_kata - x + 1)])))

    return

def kombinasi_segitiga_rata_tengah(kata):
    """
        Sekarang mari kita gabungkan dua fungsi
        segitiga spasi rata tengah
        dengan urutan kata yang sama dan input yang sama
    """

    print("\nHasil gabungan dua fungsi segitiga rata tengah:\n")

    x = 0
    panjang_kata = len(kata)

    for i in kata:
        x += 1
        print((" " * (panjang_kata - x)) + (" ".join(kata[0:x])))

    for i in kata:
        x -= 1
        print((" " * (panjang_kata - x)) + (" ".join(kata[0:(x)])))

    return

def kombinasi_segitiga_rata_tengah_2(kata):
    """
        Sekarang mari kita gabungkan dua fungsi
        segitiga spasi rata tengah dengan pola lain
        dengan urutan kata yang sama dan input yang sama
    """

    print("\nHasil gabungan dua fungsi segitiga rata tengah:\n")

    x = 0
    panjang_kata = len(kata)

    for i in kata:
        x += 1
        print((" " * (panjang_kata - x)) + (" ".join(kata[0:x])))

    for i in kata:
        x += 1
        print((" " * (panjang_kata)) + (" " * (x - 2)) +
            (" ".join(kata[(panjang_kata - (x - panjang_kata)):(panjang_kata * -1 - 1):-1]))
        )

    return

def segitiga_gunung_kembar(kata):
    """
        Sekarang mari kita buat dua gunung kembar
        dari kata segitiga dengan spasi rata tengah
        dengan urutan kata yang sama dan input yang sama
    """

    print("\nHasil segitiga gunung:\n")

    x = 0
    panjang_kata = len(kata)

    for i in kata:
        x += 1
        print(
            (" " * (panjang_kata - x)) +
            (" ".join(kata[0:x])) + 
            ((" " * (panjang_kata - x)) * 2 + " ") +
            (" ".join(kata[0:x]))
        )

    return

def segitiga_gunung_kembar_terbalik(kata):
    """
        Sekarang mari kita buat dua gunung kembar terbalik
        dari kata segitiga dengan spasi rata tengah
        dengan urutan kata yang sama dan input yang sama
    """

    print("\nHasil segitiga gunung:\n")

    x = 0
    panjang_kata = len(kata)

    for i in kata:
        x += 1
        print(
            (" " * (panjang_kata - x)) +
            (" ".join(kata[0:x])) + 
            ((" " * (panjang_kata - x)) * 2 + " ") +
            (" ".join(kata[(panjang_kata - (panjang_kata - x + 1)):(panjang_kata * -1 - 1):-1]))
        )

    return

kata = str(input("Masukkan sebuah kata: "))

## Eksekusi segitiga kata
segitiga_sumber(kata)
segitiga_terbalik(kata)
segitiga_spasi_rata_tengah(kata)
segitiga_spasi_rata_tengah_terbalik(kata)
kombinasi_segitiga_rata_tengah(kata)
kombinasi_segitiga_rata_tengah_2(kata)
segitiga_gunung_kembar(kata)
segitiga_gunung_kembar_terbalik(kata)