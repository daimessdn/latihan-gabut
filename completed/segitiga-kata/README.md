# Algoritma Segitiga Kata

## Sumber Soal (Berupa Video TikTok)

https://www.tiktok.com/@dani_lukman/video/7161585442431814939

## Pengantar

Soal atau tantangan ini diambil dari salah satu video tiktok dengan nama akun [@dani_lukman](https://www.tiktok.com/@dani_lukman). Berdasarkan kode yang beliau buat dengan bahasa pemrograman Python, ia menampilkan segitiga dari kata "python" sebagai berikut.

![](/media/dimaswehhh/398C-2E66/Kodingan/segitiga-kata/soal-python.png)

Untuk membuat kode ini lebih menarik, saya ingin mencoba variasi berbagai segitiga kata tersebut dengan pola yang menarik sebagaimana output berikut.

Berkas (*file*) ini dibentuk dengan input external berupa string "segitiga" sebagaimana terdapat di `input.in`. Dengan mengetikkan kode berikut di terminal (Linux, Python):

```bash
python3 segitiga-kata.py < input.in
```

Maka akan muncul output sebagaimana terdapat pada video-video berikut

## Output

### Segitiga dari Sumber Soal

https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga1.mp4

### Segitiga dari Sumber Soal Terbalik

https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga2.mp4

### Segitiga dengan Spasi dan Rata Tengah

https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga3.mp4

### Segitiga dengan Spasi dan Rata Tengah Terbalik

https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga4.mp4

### Kombinasi Dua Segitiga dengan Spasi dan Rata Tengah

https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga5.mp4

### Kombinasi Dua Segitiga dengan Spasi dan Rata Tengah 2

https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga6.mp4

### Segitiga Gunung Kembar

https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga7.mp4

### Segitiga Gunung Kembar Terbalik

https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga8.mp4

## Python
https://gitlab.com/daimessdn/latihan-gabut/-/blob/main/completed/segitiga-kata/segitiga-kata.py