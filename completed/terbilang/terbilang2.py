# inisiasi konstanta
SEPULUH = 10
SERATUS = 100
SERIBU = 1_000
SATU_JUTA = 1_000_000
SATU_MILIAR = 1_000_000_000
SATU_TRILIUN = 1_000_000_000_000

bilangan = ["", "satu ", "dua ", "tiga ", "empat ", "lima ", "enam ", "tujuh ", "delapan ", "sembilan ", "sepuluh ", "sebelas "]

def terbilang(n):
    """
        Fungsi konversi angka ke dalam kata terbilang

        Input: n:int
        Output: result:str
    """

    result = ""
    # 0 - 11
    if n >= 0 and n <= 11:
        result = result + bilangan[n]

    # 12 - 19
    elif n < SEPULUH * 2:
        result = terbilang(n % SEPULUH) + "belas"

    # 29 - 99
    elif n < SERATUS :
        result = terbilang(n // SEPULUH) + "puluh " + terbilang(n % SEPULUH)

    # 100 - 199
    elif n < SERATUS * 2:
        result = "seratus " + terbilang(n - SERATUS)

    # 200 - 999
    elif n < SERIBU :
        result = terbilang(n // SERATUS) + "ratus " + terbilang(n % SERATUS)

    # 1_000 - 1_999
    elif n < SERIBU * 2:
        result = "seribu " + terbilang(n - SERIBU)

    # 2_000 - 999_999
    elif n < SATU_JUTA:
        result = terbilang(n // SERIBU) + "ribu " + terbilang(n % SERIBU)

    # 1_000_000 - 999_999_999
    elif n < SATU_MILIAR:
        result = terbilang(n // SATU_JUTA) + "juta " + terbilang(n % SATU_JUTA)

    # 1_000_000_000 - 999_999_999_9999
    elif n < SATU_TRILIUN:
        result = terbilang(n // SATU_MILIAR) + "miliar " + terbilang(n % SATU_MILIAR)

    # melebihi 1 triliun
    else:
        print("Input tidak valid")
        return
    
    return result

n = terbilang(int(input("Masukkan angka: ")))

if (n != None):
    print(n + "rupiah")