# Luas Lingkaran

## Gambaran Umum

Diberikan input jari-jari (r), tentukan luas lingkaran.

## Catatan

Program pada `circle2.for` merupakan program yang sama sebagaimana `circle.for` dengan menggunakan keyword `parameter` agar mudah dipahami (berdasarkan [https://web.stanford.edu/class/me200c/tutorial_77/05_variables.html](https://web.stanford.edu/class/me200c/tutorial_77/05_variables.html)).

## Input

`r:real`

## Output

`luas:real`

## Instruksi Menjalankan Kode Fortran (Linux)

```bash
gfortran -o luas circle.for
./luas
```

## Instruksi Menjalankan Kode Fortran (Windows)

Jika aplikasi CodeBlocks beserta **compiler Fortran** sudah diinstall, Anda bisa meng-*klik* ikon tombol *play* untuk melakukan proses *compile* dan menjalankan program dengan ektensi `.exe` di sana.
