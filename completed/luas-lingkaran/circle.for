      PROGRAM lingkaran
      REAL r, luas
 
c Membaca input r (jari-jari) dan
c menghitung luas lingkaran
 
      WRITE (*,*) 'Masukan jari-jari (r): '
      READ  (*,*) r
      luas = 3.14159 * r * r
      WRITE (*,*) 'Luas Lingkaran = ', luas
 
      STOP
      END