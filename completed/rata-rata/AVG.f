      program avg

c     Program mencari rata-rata N bilangan

      integer N, i
      real jumlah, bil, rata2
      
      write(*,*) 'Masukan N bilangan: '
      read(*, *) N

      jumlah = 0

      do 100, i = 1, N
            write(*,*) 'Masukan bilangan ke-',i,': '
            read(*, *) bil
            jumlah = jumlah + bil
100   continue

      rata2 = jumlah / N

      write(*,*) 'Rata-rata',N,' bilangan: ',rata2

      stop
      end

