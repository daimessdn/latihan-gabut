# Melihat Kode Promo

## Gambaran Umum

Ketika kita sedang belanja online, pastinya disediakan berbagai promo yang menggiurkan. Pada persoalan kali ini, kita akan melihat promo apa saja yang bisa dipakai ketika membeli produk tertentu. Adapun ketentuan promo yang bisa dipakai adalah sebagai berikut

1. Memenuhi **minimal pembelian** tertentu

2. Membeli **jenis atau merek barang** tertentu.

3. Kombinasi dari dua ketentuan sebelumnya.

Pada persoalan kali ini, kita akan membuat list produk dan promo yang tersedia. Kita akan melakukan proses pembelian, kemudian ketika sudah melakukan proses pembelian, kita bisa melihat promo yang bisa digunakan (saat ini belum bisa memilih kode promo dan mengakumulasi diskon berdasarkan promo).

Pada versi selanjutnya, kita akan bisa memilih kode promo dan mengakumulasi harga dan diskon berdasarkan kode promo yang digunakan.

## Output

Terlampir pada video `kode-promo.mp4`.

## Bahasa yang Digunakan

Python
