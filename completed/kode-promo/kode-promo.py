# Program Kode Promo
## Sistem belanja dengan menggunakan promo

# fungsi validasi total nama produk

def validateProductName(name):
    sama = True

    for i in produk_dibeli:
        if not(name in i["nama"].lower()):
            sama = False

            break

    return sama

# init variabel
total_harga = 0
produk_dibeli = []

produk = [
    {
        "nama": "Xiaomi Redmi 4A",
        "harga": 1_200_000
    },
    {
        "nama": "realme Buds 2",
        "harga": 200_000
    },
    {
        "nama": "Casing HP Samsung A32",
        "harga": 20_000
    },
    {
        "nama": "iPhone 14",
        "harga": 20_000_000
    },
    {
        "nama": "Headset Samsung",
        "harga": 15_000
    },
    {
        "nama": "USB Sandisk 128GB",
        "harga": 280_000
    },
    {
        "nama": "Powerbank 10000 mAh",
        "harga": 90_000
    },
    {
        "nama": "Samsung A32",
        "harga": 3_200_000
    }
]

voucher_promo = [
    {
        "kode": "HEMATBANGET",
        "syarat": total_harga >= 100_000,
        "besar_diskon": .3,
        "max_diskon": 30_000,
        "text": "Diskon 30% dengan minimal belanja 100.000 dan maksimal diskon 30.000"
    },
    {
        "kode": "HEMATBANGET2",
        "syarat": total_harga >= 300_000,
        "besar_diskon": .3,
        "max_diskon": 50_000,
        "text": "Diskon 30% dengan minimal belanja 300.000 dan maksimal diskon 50.000"
    },
    {
        "kode": "HEMATSAMSUNG",
        "syarat": total_harga >= 1_000_000 and validateProductName("samsung"),
        "besar_diskon": .15,
        "max_diskon": 300_000,
        "text": "Diskon 30% dengan minimal belanja 1.000.000 dan maksimal diskon 300.000 khusus produk Samsung"
    },
    {
        "kode": "IPHONEHEMAT",
        "syarat": total_harga >= 5_000_000 and validateProductName("iphone"),
        "besar_diskon": .50,                 # 30%
        "max_diskon": 500_000,
        "text": "Diskon 50% dengan minimal belanja 5.000.000 dan maksimal diskon 500.000 khusus produk iPhone"
    },
]

def updateStatusPromo():
    """
    Fungsi (prosedur) update status promo
    """
    
    global voucher_promo

    refresh_promo = [
        {
        "kode": "HEMATBANGET",
        "syarat": total_harga >= 100_000,
        "besar_diskon": .3,
        "max_diskon": 30_000,
        "text": "Diskon 30% dengan minimal belanja 100.000 dan maksimal diskon 30.000"
    },
    {
        "kode": "HEMATBANGET2",
        "syarat": total_harga >= 300_000,
        "besar_diskon": .3,
        "max_diskon": 50_000,
        "text": "Diskon 30% dengan minimal belanja 300.000 dan maksimal diskon 50.000"
    },
    {
        "kode": "HEMATSAMSUNG",
        "syarat": total_harga >= 1_000_000 and validateProductName("samsung"),
        "besar_diskon": .15,
        "max_diskon": 300_000,
        "text": "Diskon 30% dengan minimal belanja 1.000.000 dan maksimal diskon 300.000 khusus produk Samsung"
    },
    {
        "kode": "IPHONEHEMAT",
        "syarat": total_harga >= 5_000_000 and validateProductName("iphone"),
        "besar_diskon": .50,                 # 30%
        "max_diskon": 500_000,
        "text": "Diskon 50% dengan minimal belanja 5.000.000 dan maksimal diskon 500.000 khusus produk iPhone"
    },
    ]

    for i, j in enumerate(voucher_promo):
        j["syarat"] = refresh_promo[i]["syarat"]

    return

toko_start = True

print("SELAMAT DATANG DI TOKO HENGPON")
print("==============================")
print("List Barang:")

for j, i in enumerate(produk):
    print((str(j + 1) + ". " + i["nama"] + ":"), i["harga"])

print("==============================")
print("List Kode Promo:")
for j, i in enumerate(voucher_promo):
    print((str(j + 1) + ". " + i["kode"] + ":"), i["text"])

print("==============================")


while(toko_start == True):
    x = int(input("Input nomor barang yang mau dibeli (0 untuk sudahi pembelian): "))
    
    if (x > len(produk)):
        print("Tidak ada produk yang dimaksud.")
        x = int(input("Input nomor barang yang mau dibeli: "))

    elif (x == 0):
        toko_start = False

    else:
        produk_dibeli.append(produk[x - 1])

# kalkulasi total belanja
print("==============================")
print("Struk Pembelian")
for i in produk_dibeli:
    print(i["nama"] + ":", i["harga"])
    total_harga += i["harga"]

print("==============================")
print("Total pembelian:", total_harga)
print("==============================")

updateStatusPromo()

# promo yang bisa dipakai
print("==============================")
print("Kode yang bisa dipakai:", ", ".join([x["kode"] for x in voucher_promo if x["syarat"] == True]))
print("==============================")
