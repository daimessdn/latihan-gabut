      program LKPSG

c     program menghitung luas dan keliling persegi

      integer s, kllg, luas

      write(*,*) 'Masukan sisi persegi: '
      read(*, *) s

      kllg = s * 4
      luas = s * s

      write(*, *) 'Luas persegi: ', luas
      write(*, *) 'Keliling persegi: ', kllg

      stop
      end