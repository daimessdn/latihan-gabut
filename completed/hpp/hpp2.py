# Program HPP
## Menghitung Nilai Harga Pokok Penjualan Perusahaan Dagang

# Contoh 2
## Terdapat data toko pakaian

"""
Sebuah toko yang menjual pakaian sedang menyusun laporan keuangan akhir tahun dan menghitung jumlah persediaan seperti informasi berikut:

Persediaan awal barang tahun 2021 = Rp500.000.000,00
Pembelian baru selama tahun 2021 = Rp800.000.000,00
Persediaan akhir barang tahun 2021 = Rp600.000.000,00
"""

## Sumber: https://www.jurnal.id/id/blog/apa-yang-dimaksud-harga-pokok-penjualan-hpp/

# inisiasi variabel
## persediaan awal
persediaan_awal = 500_000_000
print("\n" + ("-" * 15))
print("Persediaan Awal:", persediaan_awal)

## pembelian bersih
"""
Dengan mengasumsikan pembelian baru barang
sebagai pembelian bersih, maka
ongkos, retur, serta potongan diabaikan (dibuat nol)
"""

pembelian = 800_000_000
ongkos_pembelian = 0

print("\n" + ("-" * 15))
print("Pembelian:", pembelian)
print("Ongkos Pembelian:", ongkos_pembelian)

potongan_pembelian = 0
retur_pembelian = 0

print("\nRetur Pembelian:", pembelian)
print("Potongan Pembelian:", ongkos_pembelian)

"""
Perlu diingat bahwa pembelian bersih = 
pembelian + ongkos - (retur + potongan) sehingga
"""
pembelian_bersih = pembelian + ongkos_pembelian - (retur_pembelian + potongan_pembelian)

print("\nPembelian Bersih:", pembelian)

## persediaan barang
"""
Perlu diingat bahwa persediaan barang = 
pembelian bersih + persediaan awal sehingga
"""
barang_tersedia = persediaan_awal + pembelian_bersih

print("\n" + ("-" * 15))
print("Barang Tersedia:", barang_tersedia)

## persediaan akhir
persediaan_akhir = 600_000_000

print("\n" + ("-" * 15))
print("Persediaan Akhir:", persediaan_akhir)

# kalkulasi HPP
"""
HPP = pembelian bersih - (persediaan awal - persediaan akhir)
"""
def hpp(pembelian_bersih, stok_awal, stok_akhir):
	return pembelian_bersih + (stok_awal - stok_akhir)

# output
print("\nsehingga HPP = " + 
	str(hpp(pembelian_bersih, persediaan_awal, persediaan_akhir)))