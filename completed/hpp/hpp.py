# Program HPP
## Menghitung Nilai Harga Pokok Penjualan Perusahaan Dagang

# Contoh 1
## Terdapat data HPP UD. ANDI per 31 Maret 2022
## Sumber: https://www.jurnal.id/id/blog/apa-yang-dimaksud-harga-pokok-penjualan-hpp/

# inisiasi variabel
## persediaan awal
persediaan_awal = 15_000_000
print("\n" + ("-" * 15))
print("Persediaan Awal:", persediaan_awal)

## pembelian bersih
pembelian = 75_000_000
ongkos_pembelian = 1_000_000

print("\n" + ("-" * 15))
print("Pembelian:", pembelian)
print("Ongkos Pembelian:", ongkos_pembelian)

potongan_pembelian = 4_000_000
retur_pembelian = 0

print("\nRetur Pembelian:", pembelian)
print("Potongan Pembelian:", ongkos_pembelian)

"""
Perlu diingat bahwa pembelian bersih = 
pembelian + ongkos - (retur + potongan) sehingga
"""
pembelian_bersih = pembelian + ongkos_pembelian - (retur_pembelian + potongan_pembelian)

print("\nPembelian Bersih:", pembelian)

## persediaan barang
"""
Perlu diingat bahwa persediaan barang = 
pembelian bersih + persediaan awal sehingga
"""
barang_tersedia = persediaan_awal + pembelian_bersih

print("\n" + ("-" * 15))
print("Barang Tersedia:", barang_tersedia)

## persediaan akhir
persediaan_akhir = 12_500_000

print("\n" + ("-" * 15))
print("Persediaan Akhir:", persediaan_akhir)

# kalkulasi HPP
"""
HPP = pembelian bersih - (persediaan awal - persediaan akhir)
"""
def hpp(pembelian_bersih, stok_awal, stok_akhir):
	return pembelian_bersih + (stok_awal - stok_akhir)

# output
print("\nsehingga HPP = " + 
	str(hpp(pembelian_bersih, persediaan_awal, persediaan_akhir)))