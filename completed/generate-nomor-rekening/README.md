# Meng-*generate* nomor rekening

## Gambaran Umum

Pada persoalan kali ini, kita diminta untuk membuat (kode) nomor rekening dengan panjang karakter sebanyak N karakter.

Terdapat dua versi yang diselesaikan pada persoalan kali ini: *generate* nomor rekening dengan nomor desimal (0-9), serta heksadesimal (0-f).

## Input

`N:int`

## Output

`result:string`

*Adapun output terlampir pada video TikTok oleh [@d.ganteng21](https://www.tiktok.com/@d.ganteng21/video/7188028167628573979)

## Bahasa yang Digunakan

Python
