# PROGRAM GENARATE NOMOR REKENING/ID REKENING
## Generate nomor kartu kredit secara acak dengan menggunakan random

import random

def generate_nomor_rekening_2(N):
    """
    Membuat id nomor rekening berdasarkan karakter acak
    sepanjang N karakter

    input: N:int
    output: result:str
    """

    # init list hexadecimal
    hexadecimal_list = ['0', '1', '2', '3',
                        '4', '5', '6', '7',
                        '8', '9', 'a', 'b',
                        'c', 'd', 'e', 'f']

    result = "".join([str(random.choice(hexadecimal_list)) for i in range(N)])
    return result

a = int(input("Masukkan jumlah digit nomor rekening: "))
print("Nomor rekening %d digit: %s" % (a, generate_nomor_rekening_2(a)))