# PROGRAM GENARATE NOMOR REKENING/ID REKENING
## Generate nomor kartu kredit secara acak dengan menggunakan random

import random

def generate_nomor_rekening(N):
    """
    Membuat id nomor rekening berdasarkan nomor acak
    sepanjang N karakter

    input: N:int
    output: result:str
    """

    result = "".join([str(random.randint(0, 9)) for i in range(N)])
    return result

a = int(input("Masukkan jumlah digit nomor rekening: "))
print("Nomor rekening %d digit: %s" % (a, generate_nomor_rekening(a)))