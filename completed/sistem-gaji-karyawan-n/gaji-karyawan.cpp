#include <iostream>
using namespace std;

int main() {
    // input n karyawan
    int n = 0;
    cout << "Input n karyawan: ";
    cin >> n;

    cout << "\nAda " << n << " karyawan\n";

    // jika diisi dengan bilangan 0 dan negatif,
    //// maka harus isi ulang jumlah karyawan
    while (n <= 0) {
        cout << "Setidaknya harus ada 1 (satu) karyawan.\n";
        
        cout << "Input n karyawan: ";
        cin >> n;
    }

    int i = 1; // inisiasi iterasi karyawan
    int total_gaji_semua_karyawan;

    /*
        Perlu diingat bahwa do while digunakan untuk
        proses looping jika suatu kondisi memenuhi.
        Proses akan dijalankan setidaknya satu kali, baru validasi while.
        Ketika while memenuhi, maka proses berlanjut. Adapun jika tidak memenuhi, proses akan berhenti.
    */
    do {
        cout << "\nHitung gaji karyawan ke-" << i << "\n";
        
        // inisiasi jumlah lembur, punya istri, dan punya anak
        int punya_istri; int jam_lembur; int jumlah_anak;

        // inisiasi gaji dan tunjangan 1 karyawan
        int gaji_karyawan = 0;
        int gaji_pokok = 0;
        int gaji_lembur = 0;
        int transport = 0;
        int tunjangan_istri = 0;
        int tunjangan_anak = 0;

        cout << "Jam lembur: ";
        cin >> jam_lembur;

        cout << "Punya istri? (tulis 1 untuk ya atau 2 untuk tidak): ";
        cin >> punya_istri;

        cout << "Jumlah anak: ";
        cin >> jumlah_anak;

        // dengan asumsi 1 bulan = 30 hari,
        //// maka tarif 100 ribu dikalikan 30 hari
        gaji_pokok = 100000 * 30;
        
        // kalkulasi gaji lembur
        gaji_lembur = jam_lembur * 25000;

        // kalkulasi transport
        if (jam_lembur > 10) {
            transport = int(gaji_lembur * 0.1);
        }

        // kalkulasi tunjangan istri
        if (punya_istri == 1) {
            tunjangan_istri = int(gaji_pokok * 0.15);
        }

        // kalkulasi tunjangan anak
        if (jumlah_anak >= 1 && jumlah_anak <= 2) {
            tunjangan_anak = int(gaji_pokok * 0.1 * jumlah_anak);
        } else if (jumlah_anak > 2) {
            tunjangan_anak = int(gaji_pokok * 0.1 * 2);              // hanya sampai anak kedua saja
        }

        // kalkukasi semua gaji
        gaji_karyawan = gaji_pokok + gaji_lembur + transport + tunjangan_istri + tunjangan_anak;

        cout << "Gaji karyawan ke-" << i << ": " << gaji_karyawan << "\n";
        total_gaji_semua_karyawan += gaji_karyawan;

        i++; // lanjut ke karyawan berikutnya
    } while (i <= n);
    
    cout << "\n\n======================================\n";
    cout << "Total gaji yang harus dibayarkan untuk " << n << " karyawan: " << total_gaji_semua_karyawan<< "\n";

    return 0;
}
