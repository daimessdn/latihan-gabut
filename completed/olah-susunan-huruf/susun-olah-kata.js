var kataInput = "Sample Case".toLowerCase()

var arrayKata = kataInput.split("").filter(x => x !== " ")

var kataSudahDicek = []
var kataSudahDicekCount = []

var kataOutput = ""

for (var i = 0; i < arrayKata.length; i++) {
    if (!kataSudahDicek.includes(arrayKata[i])) {
        kataSudahDicek.push(arrayKata[i]);
        kataSudahDicekCount.push(arrayKata.reduce((x, a) => { return x = a == arrayKata[i] ? x + 1 : x }, 0));
    }
}

for (var i = 0; i < kataSudahDicek.length; i++) {
    for (var j = 0; j < kataSudahDicekCount[i]; j++) {
        kataOutput += kataSudahDicek[i];
    }
}

console.log(kataOutput);