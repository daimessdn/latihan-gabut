# init kata
# kata_input = str(input("Masukkan sebuah kata: ")).lower()
kata_input = "Sample Case".lower()

array_kata = [x for x in kata_input if x != " "]

kata_sudah_dicek = []
kata_sudah_dicek_count = []

kata_output = ""

for i in range(len(array_kata)):
    if not(array_kata[i] in kata_sudah_dicek):
        kata_sudah_dicek.append(array_kata[i])
        kata_sudah_dicek_count.append(array_kata.count(array_kata[i]))

# print(kata_sudah_dicek, kata_sudah_dicek_count)

for i in range(len(kata_sudah_dicek)):
    kata_output += kata_sudah_dicek[i] * kata_sudah_dicek_count[i]

print(kata_output)