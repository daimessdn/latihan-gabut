import pygame
import time
import random

pygame.init()

#  init warna
putih = (255, 255, 255)
kuning = (255, 255, 102)
hitam = (0, 0, 0)
merah = (213, 50, 80)
hijau = (0, 255, 0)
biru = (50, 153, 213)

# dimensi game
lebar = 600
tinggi = 400

# bikin mode dan caption title bar
dis = pygame.display.set_mode((lebar, tinggi))
pygame.display.set_caption('Game Ular oleh Rio')

Clock = pygame.time.Clock()

# kecepatan dan ukuran blok ular
blok_ular = 10
kecepatan_ular = 10

# font yang dipakai
gaya_tulisan = pygame.font.SysFont("arial", 25)
gaya_skor = pygame.font.SysFont("arial", 35)


def ular_kita(kepala_ular, daftar_ular, blok_ular):
    pygame.draw.rect(dis, hitam, [kepala_ular[0], kepala_ular[1], blok_ular, blok_ular])

    for x in daftar_ular:
        pygame.draw.rect(dis, hitam, [x[0], x[1], blok_ular, blok_ular])


def pesan(pesan, warna):
    pesan = gaya_tulisan.render(pesan, True, warna)
    dis.blit(pesan, [int(lebar / 6), int(tinggi / 3)])


def gameLoop():
    game_over = False
    game_close = False

    x1 = int(lebar / 2)
    y1 = int(tinggi / 2)

    x1_ubah = 0
    y1_ubah = 0

    daftar_ular = []
    panjang_ular = 1

    makananx = int(round(random.randrange(0, lebar - blok_ular) / 10.0) * 10.0)
    makanany = int(round(random.randrange(0, tinggi - blok_ular) / 10.0) * 10.0)

    while not game_over:
        while game_close == True:
            dis.fill(biru)
            gaya_tulisan.render("Kamu Kalah! Tekan C-Play Lagi atau Q-Quit", True, merah)
            gaya_skor.render("Skor: " + str(panjang_ular), True, merah)

            pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    game_over = True
                    game_close = False
                if event.key == pygame.K_c:
                    game_close = False
                    gameLoop()

                if event.type == pygame.QUIT:
                    game_over = True

                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT and x1_ubah != blok_ular * -1:
                        x1_ubah = blok_ular * -1
                        y1_ubah = 0
                    elif event.key == pygame.K_RIGHT and x1_ubah != blok_ular:
                        x1_ubah = blok_ular
                        y1_ubah = 0
                    elif event.key == pygame.K_UP and y1_ubah != blok_ular * -1:
                        y1_ubah = blok_ular * -1
                        x1_ubah = 0
                    elif event.key == pygame.K_DOWN and y1_ubah != blok_ular:
                        y1_ubah = blok_ular
                        x1_ubah = 0
 
        if x1 >= lebar or x1 < 0 or y1 >= tinggi or y1 < 0:
            game_close = True

        x1 += x1_ubah
        y1 += y1_ubah

        dis.fill(biru)

        # tambah makanan
        pygame.draw.rect(dis, hijau, [makananx, makanany, blok_ular, blok_ular])

        kepala_ular = [x1, y1]
        # kepala_ular.append(x1)
        # kepala_ular.append(y1)

        print("ular", x1, y1, "makanan", makananx, makanany, "daftar ular", daftar_ular)

        daftar_ular.append([x1 - x1_ubah, y1 - y1_ubah])

        if len(daftar_ular) > panjang_ular:
            del daftar_ular[0]

        for x in daftar_ular[:-1]:
            if x == kepala_ular:
                game_close = True
 
        ular_kita(kepala_ular, daftar_ular, blok_ular)

        pygame.display.update()
 
        if x1 == makananx and y1 == makanany:
            makananx = int(round(random.randrange(0, lebar - blok_ular) / 10.0) * 10.0)
            makanany = int(round(random.randrange(0, tinggi - blok_ular) / 10.0) * 10.0)

            daftar_ular.append([x1 - x1_ubah, y1 - y1_ubah])
            panjang_ular += 1

            print("kena makanan")
            print(daftar_ular)
            
        Clock.tick(kecepatan_ular)
 
    pygame.quit()
    quit()
 
gameLoop()

# while True:
#     print("test")
 