# Program Mencari Ranjau
## Membantu Mas Ilham mencari ranjau

# init variabel
ukuran_area = [5, 5] # 5 x 5

# BUAT SKENARIO SEBELUM ADANYA RANJAU
print("Area sebelum dianalisis:\n")

# swiping ranjau baris (sumbu-Y)
for i in range (ukuran_area[1]):
    init_baris = "";

    # swiping ranjau kolom (sumbu-X)
    for j in range(ukuran_area[0]):
        """
            Isi semua ranjau dengan 0 karena belum dianalisis
        """
    
        init_baris += "0"
        
        if (j >= 0):
            init_baris += " "

    # output baris
    print(init_baris)


# ALGORITMA MANUAL
print("\nArea setelah dianalais: cara manual")

# swiping ranjau baris (sumbu-Y)
for i in range (ukuran_area[1]):
    init_baris = ""

    # swiping ranjau kolom (sumbu-X)
    for j in range(ukuran_area[0]):
        """
            Diketahui bahwa jumlah koordinat
            sumbu-X dan sumbu-Y yang habis dibagi tiga
            dan bukan angka nol mengindikasikan
            adanya ranjau, sehingga
        """

        if ((i + j) % 3 == 0 and (i + j) != 0):
            init_baris += "1"
        else:
            init_baris += "0"
        
        # penambahan spasi
        if (j >= 0):
            init_baris += " "

    # output baris
    print(init_baris)

# CARA CEPET: MENGGUNAKAN TERNARY OPERATOR
print("\nArea setelah dianalais: cara cepet menggunakan ternary operator")

# swiping ranjau baris (sumbu-Y)
for i in range (ukuran_area[1]):
    init_baris = ""

    # swiping ranjau kolom (sumbu-X)
    for j in range(ukuran_area[0]):
        """
            Dengan kasus yang sama, maka
        """

        init_baris += "1" if ((i + j) % 3 == 0 and (i + j) != 0) else "0"
        init_baris += " " if j >= 0 else ""

    # output baris
    print(init_baris)

# CARA CEPET: MENGGUNAKAN KORESPONDENSI ARRAY
print("\nArea setelah dianalais: cara cepet\
 menggunakan array korespondensi dan fungsi join")

for i in range(ukuran_area[1]):
    print(" ".join([
        ("1" if ((i + j) % 3 == 0 and (i + j) != 0) else "0")
        for j in range(ukuran_area[0])
    ]))