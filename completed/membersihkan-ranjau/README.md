# Membersihkan Ranjau

## Pengantar

Berdasarkan soal yang terdapat di `soal.jpeg`, diketahui Ilham dan kawan-kawan sedang melakukan analisis adanya ranjau di sekitar area 5 x 5. Setelah dianalisis, diketahui bahwa ranjau terdapat di setiap area dengan jumlah koordinat (sumbu-x + sumbu-y) habis dibagi tiga dan bukan nol.

Kali ini, kita akan membuat koordinat yang sebelum dianalisis dan setelah dianalisis.

## Output

### Python

```bash
$ python3 mencari-ranjau.py
```

```
Area sebelum dianalisis:

0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 

Area setelah dianalais: cara manual
0 0 0 1 0 
0 0 1 0 0 
0 1 0 0 1 
1 0 0 1 0 
0 0 1 0 0 

Area setelah dianalais: cara cepet menggunakan ternary operator
0 0 0 1 0 
0 0 1 0 0 
0 1 0 0 1 
1 0 0 1 0 
0 0 1 0 0 

Area setelah dianalais: cara cepet menggunakan array korespondensi dan fungsi join
0 0 0 1 0
0 0 1 0 0
0 1 0 0 1
1 0 0 1 0
0 0 1 0 0
```

## C++

```bash
$ g++ mencari-ranjau.cpp -o ranjau
$ ./ranjau
```

```
Area sebelum dianalisis:
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
Area setelah dianalisis:
0 0 0 1 0 
0 0 1 0 0 
0 1 0 0 1 
1 0 0 1 0 
0 0 1 0 0 
```

### JavaScript (Node)

```bash
$ node mencari-ranjau.js
```

```
Area sebelum dianalisis:
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
0 0 0 0 0 
Area setelah dianalisis: cara manual:
0 0 0 1 0 
0 0 1 0 0 
0 1 0 0 1 
1 0 0 1 0 
0 0 1 0 0 
Area setelah dianalisis: cara ternary operator:
0 0 0 1 0 
0 0 1 0 0 
0 1 0 0 1 
1 0 0 1 0 
0 0 1 0 0 
```
