// Program Mencari Ranjau
//// Membantu Mas Ilham mencari ranjau

#include <iostream>
using namespace std;

// definisi ukuran koordinat peta ranjau
#define UKURAN_X 5
#define UKURAN_Y 5

// buat prosedur looping sebelum dianalisis
void areaSebelumDianalisis(int x, int y) {
    /*
        Isi semua ranjau dengan 0 karena belum dianalisi
    */

    cout << ("Area sebelum dianalisis:") << endl;

    // swiping ranjau baris (sumbu-Y)
    for (int i = 0; i < UKURAN_Y; i++) {
        string init_baris = "";

        for (int j = 0; j < UKURAN_X; j++) {
            init_baris += "0";
            
            if (j >= 0) {
                init_baris += " ";
            }
        }

        // output baris
        cout << init_baris << endl;
    }
};

// buat prosedur looping setelah dianalisis
void areaSetelahDianalisis(int x, int y) {
    /*
        Isi semua ranjau yang telah dianalisis.
        Karena diketahui bahwa jumlah koordinat
        sumbu-X dan sumbu-Y yang habis dibagi tiga
        dan bukan angka nol mengindikasikan
        adanya ranjau, sehingga
    */

    cout << ("Area setelah dianalisis:") << endl;

    // swiping ranjau baris (sumbu-Y)
    for (int i = 0; i < UKURAN_Y; i++) {
        string init_baris = "";

        for (int j = 0; j < UKURAN_X; j++) {
            if ((i + j) != 0 && (i + j) % 3 == 0) {
                init_baris += "1";
            } else {
                init_baris += "0";
            }
            
            if (j >= 0) {
                init_baris += " ";
            }
        }

        // output baris
        cout << init_baris << endl;
    }
};

int main() {
    areaSebelumDianalisis(UKURAN_X, UKURAN_Y);
    areaSetelahDianalisis(UKURAN_X, UKURAN_Y);

    return 0;
}