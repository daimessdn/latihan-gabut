// Program Mencari Ranjau
//// Membantu Mas Ilham mencari ranjau

const KOORDINAT = [5, 5]  // 5 x 5

// buat prosedur looping sebelum dianalisis
const areaSebelumDianalisis = (x, y) => {
    /*
        Isi semua ranjau dengan 0 karena belum dianalisi
    */

    console.log("Area sebelum dianalisis:");

    // swiping ranjau baris (sumbu-Y)
    for (let i = 0; i < KOORDINAT[1]; i++) {
        let init_baris = "";

        for (let j = 0; j < KOORDINAT[0]; j++) {
            init_baris += "0";
            
            if (j >= 0) {
                init_baris += " ";
            }
        }

        // output baris
        console.log(init_baris);
    }
};

// buat prosedur looping setelah dianalisis
//// dengan cara manual
const areaSetelahDianalisisManual = (x, y) => {
    /*
        Isi semua ranjau yang telah dianalisis.
        Karena diketahui bahwa jumlah koordinat
        sumbu-X dan sumbu-Y yang habis dibagi tiga
        dan bukan angka nol mengindikasikan
        adanya ranjau, sehingga
    */

    console.log("Area setelah dianalisis: cara manual:");

    // swiping ranjau baris (sumbu-Y)
    for (let i = 0; i < KOORDINAT[1]; i++) {
        let init_baris = "";

        for (let j = 0; j < KOORDINAT[0]; j++) {
            if ((i + j) != 0 && (i + j) % 3 == 0) {
                init_baris += "1";
            } else {
                init_baris += "0";
            }
            
            if (j >= 0) {
                init_baris += " ";
            }
        }

        // output baris
        console.log(init_baris);
    }
};

// buat prosedur looping setelah dianalisis
//// dengan cara ternary operator
const areaSetelahDianalisisTernary = (x, y) => {
    /*
        Dengan kasus yang sama, maka
    */

    console.log("Area setelah dianalisis: cara ternary operator:");

    // swiping ranjau baris (sumbu-Y)
    for (let i = 0; i < KOORDINAT[1]; i++) {

        let init_baris = "";

        for (let j = 0; j < KOORDINAT[0]; j++) {
            init_baris += ((i + j) != 0 && (i + j) % 3 == 0) ? "1" : "0";
            init_baris += (j >= 0) && " ";
        }

        // output baris
        console.log(init_baris);
    }
};


areaSebelumDianalisis(KOORDINAT[0], KOORDINAT[1]);
areaSetelahDianalisisManual(KOORDINAT[0], KOORDINAT[1]);
areaSetelahDianalisisTernary(KOORDINAT[0], KOORDINAT[1]);
