# SISTEM MENGELOLA TABUNGAN

# init variable
masuk_atm = False
saldo = 0

# validasi pin ATM
pin = str(input("Masukkan PIN Anda: "))

if (pin == "123"):
    masuk_atm = True
    print("PIN benar, selamat datang di ATM BANK RUT")

else:
    print("PIN salah, keluar dari sistem")

# ketika masuk sistem ATM
while (masuk_atm == True):
    print("Saldo Anda: " + str(saldo))

    print("Pilihan menu")
    print("1. Setor tunai")
    print("2. Tarik tunai")
    print("3. Keluar")
    print("=================")

    menu = int(input("Pilih menu: "))

    # menu valid
    if (menu <= 3 and menu > 0):
        # setor tunai
        if (menu == 1):
            setor = int(input("Masukkan saldo yang ingin disetor: "))

            saldo += setor

            print("Setor tunai berhasil")
        
        # tarik tunai
        elif (menu == 2):
            tarik = int(input("Masukkan saldo yang mau ditarik: "))

            while (tarik > saldo):
                print("Saldo tidak cukup")
                tarik = int(input("Masukkan saldo yang mau ditarik: "))

            saldo -= tarik

            print("Tarik tunai berhasil")

        elif (menu == 3):
            masuk_atm = False # keluar dari sistem atm

    else:
        print("Menu tidak valid")