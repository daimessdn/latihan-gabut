print("====== SISTEM SEWA MOBIL ======")

# init variabel
total = 0
sukses_login = False
input_data_berhasil = False
biaya_lunas = False

# konstanta harga mobil
HARGA_AVANZA   = 200_000
HARGA_HILUX    = 300_000
HARGA_FORTUNER = 500_000

# fungsi sewa mobilnya
def fungsi_sewa_mobil():
    """Fungsi sewa mobil"""

    # global state init variable
    global total
    global input_data_berhasil
    global biaya_lunas

    # inisiasi tampilan sewa mobil
    print("--- WELCOME BACK ---")
    print("Perusahaan ini menyewakan 3 jenis mobil:")
    print("1. Avanza   = Rp 200.000/jam")
    print("2. Hilux    = Rp 300.000/jam")
    print("3. Fortuner = Rp 500.000/jam")
    print("--------------------\n")

    # input biodata diri
    while (input_data_berhasil == False):
        nama_ktp = str(input("Masukkan nama Anda sesuai KTP: "))
        nomor_ktp = str(input("Masukkan nomor NIK sesuai KTP: "))
        alamat_ktp = str(input("Masukkan alamat sesuai KTP: "))

        # validasi biodata diinput
        ## jika ditulis dengan benar, lanjut ke step sewa mobil
        ## jika salah, tulis lagi biodatanya
        if (nama_ktp == "Bambang" and nomor_ktp == "123" and alamat_ktp == "Bojong"):
            input_data_berhasil = True
            print("Biodata benar\n")

        else:
            print("Biodata salah, silakan input biodata dengan benar\n")

    # lanjut ke perentalan
    banyak_penyewaan = (int(input("Masukkan banyak perentalan: ")))

    i = 0 # init variabel penyewaan

    while (i < banyak_penyewaan):
        print("\nPenyewaan ke-" + str(i+1))
        
        # init biaya per penyewaan
        biaya = 0

        # input jenis dan waktu sewa mobil
        jenis = int(input("Masukkan jenis mobil (input berupa angka): "))
        waktu_sewa = int(input("Masukkan lama peminjaman (berupa jam): "))

        if (jenis == 1):
            biaya = HARGA_AVANZA * waktu_sewa
            total += biaya

            print("Biaya sewa Avanza:", biaya, "rupiah")

            i += 1  # lanjut ke penyewaan berikutnya

        elif (jenis == 2):
            biaya += HARGA_HILUX * waktu_sewa
            total += biaya

            print("Biaya sewa Hilux:", biaya, "rupiah")

            i += 1  # lanjut ke penyewaan berikutnya

        elif (jenis == 3):
            biaya += HARGA_FORTUNER * waktu_sewa
            total += biaya

            print("Biaya sewa Fortuner:", biaya, "rupiah")

            i += 1  # lanjut ke penyewaan berikutnya

        # Pada kasus bahwa jenis mobil tidak tersedia (selain nomor 1, 2, dan 3)
        ## maka tidak lanjut ke penyewaan berikutnya (misalnya penyewaan 1 tidak valid, maka akan tetap ke penyewaan 1)
        else:
            print("Pilihan tidak tersedia, tetap berada di penyewaan ke-", i+1)

    # output total bayar
    print("\nTotal semua yang harus dibayar:", total, "rupiah")
    
    # diskon 5% untuk pembelian 3 juta rupiah atawa lebih
    if (total >= 3_000_000):
        diskon = int(total * .05)
        total = int(total - diskon)

        print("\n\nKarena total harga sebesar 3 juta atau lebih,")
        print("Anda mendapatkan potongan harga sebesar", diskon, "rupiah")
        print("Sehingga total yang harus dibayar adalah", total, "rupiah")

    # pembayaran cash uang pembeli
    ## kalo uang kurang, maka tidak cukup dan input lagi uang cash
    ## kalo uang cukup, maka pembayaran lunas dan tampilkan kembaliannya
    while (biaya_lunas == False):
        uang_cash = int(input("\nUang cash pembeli (dalam rupiah): "))

        if (uang_cash < total):
            print("Uang tidak cukup")

        else:
            biaya_lunas = True
            print("Pembayaran lunas")

            kembalian = uang_cash - total
            print("\nKembalian:", kembalian, "rupiah")

    return

# input username dan password
username = str(input("Masukkan username: "))
password = str(input("Masukkan password: "))

# validasi username dan password
if (username == "bambang" and password == "123"):
    sukses_login = True
    print("Login berhasil\n")

else:
    print("Login gagal")

# ketika berhasil login
if (sukses_login == True):
    fungsi_sewa_mobil()           # fungsi sewa mobil dipanggil
